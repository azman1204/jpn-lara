@extends('master')
@section('content')

<form method="post" action="{{ url('/cari') }}">
    @csrf
    Title : <input type="text" name="title">
    <input type="submit" value="Search">
</form>

<a href="{{ url('/post/create') }}">New Post</a>

<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Bil</th>
            <th>Tajuk</th>
            <th>Tindakan</th>
        </tr>
    </thead>
    <tbody>
        @php $no = $posts->firstItem() @endphp
        @foreach($posts as $post)
        <tr>
            <td>{{ $no++ }} {{-- $loop->iteration --}}</td>
            <td>{{ $post->title }}</td>
            <td>
                <a href="{{ url('/post/'.$post->id.'/edit') }}">Edit</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

{{ $posts->links() }}

@endsection
