@extends('master')
@section('content')

@foreach($errors->all() as $err)
    <div class="text text-danger">{{ $err }}</div>
@endforeach

<form method="post" action="{{ url('/post') }}" onsubmit="validateMe()">
    @csrf
    Title
    <br>
    <input type="text" name="title" value="{{ old('title') }}">
    <br>
    Description
    <br>
    <textarea name="description">{{ old('description') }}</textarea>
    <br>
    <input type="submit">
</form>

<script>
function validateMe() {
    alert('ok');
    // validation js disini
}
</script>

@endsection
