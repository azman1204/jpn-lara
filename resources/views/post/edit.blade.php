@extends('master')
@section('content')

@foreach($errors->all() as $err)
    <div class="text text-danger">{{ $err }}</div>
@endforeach

<form method="post" action="{{ url('/post/' . $post->id) }}">
    @method('PUT')
    @csrf
    Title
    <br>
    <input type="text" name="title" value="{{ old('title', $post->title) }}">
    <br>
    Description
    <br>
    <textarea name="description">{{ old('description', $post->description) }}</textarea>
    <br>
    <input type="submit">
</form>

<form method="post" action="{{ url('/post/'.$post->id) }}">
    @method('DELETE')
    @csrf
    <input type="submit" value="Delete">
</form>

@endsection
