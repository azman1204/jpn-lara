<?php

use App\Http\Controllers\Dashboard\PostController;
use App\Http\Controllers\SampleController;
use Illuminate\Support\Facades\Route;

// http://jpn-lara.test/
Route::get('/', function () {
    return view('welcome'); // ../views/welcome.blade.php
});

// http://jpn-lara.test/hello
Route::get('/hello', function() {
    return view('hello', ['name' => 'Azman']);
});

// route terus return result
Route::get('/test1', function() {
    return "Assalamualaikum ...";
});

// http://jpn-lara.test/demo1
Route::get('/demo1', [SampleController::class, 'demo1']);

// http://jpn-lara.test/demo2
Route::get('/demo2', [SampleController::class, 'demo2']);

// http://jpn-lara.test/demo3
Route::get('/demo3', [SampleController::class, 'demo3']);

Route::resource('/post', PostController::class);

Route::post('/cari', [PostController::class, 'cari']);
