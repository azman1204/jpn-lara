<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('post', function (Blueprint $table) {
            $table->id(); // id int pk
            $table->string("title");
            $table->string("slug");
            $table->text("content");
            $table->integer("category_id");
            $table->string("description");
            $table->enum("posted", ['yes', 'not']);
            $table->string("image");
            $table->timestamps(); // created_at, updated_at (timestamp)
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('post');
    }
};
