<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function cari(Request $req) {
        $title = $req->title;
        $posts = Post::where('title', 'like', "%$title%")
        ->paginate(1);
        return view('post.list', ['posts' => $posts]);
    }

    // GET jpn-lara.test/post
    public function index()
    {
        $posts = Post::paginate(1); //return array of Post obj
        //$posts = Post::all(); //return array of Post obj
        //return "listing posts";
        return view('post.list', ['posts' => $posts]);
    }

    // jpn-lara.test/post/create
    public function create()
    {
        // return "show form";
        return view('post.form');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // form validation
        $request->validate([
            'title' => 'required|min:5|max:255',
            'description' => 'required|min:5',
        ]);
        //dd($request->all());
        //return "save data";
        Post::create([
            'title'       => $request->title,
            'description' => $request->description,
            'image'       => 'test.jpg',
            'posted'      => 'yes',
            'slug'        => 'test-abc-def',
            'category_id' => 1,
            'content'     => ''
        ]);
        //return "data has been saved!";
        return redirect('/post');
    }

    /**
     * Display the specified resource.
     */
    public function show(Post $post)
    {
        dd($post);
        return "showing data..";
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Post $post)
    {
        //return "editing post " . $post->title;
        //dd($post);
        return view('post.edit', ['post' => $post]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Post $post)
    {
        // form validation
        $request->validate([
            'title' => 'required|min:5|max:255',
            'description' => 'required|min:5',
        ],[
           'title.required' => 'Tajuk wajib diisi',
           'title.min' => 'Minima tajuk ialah 5 karakter'
        ]
    );

        $post->title = $request->title;
        $post->description = $request->description;
        $post->save();
        //return "data updated..";
        return redirect('/post')
        ->with('status', 'Data telah berjaya dikemaskini');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Post $post)
    {
        $post->delete();
        //return "data deleted..";
        return redirect('/post');
    }
}
