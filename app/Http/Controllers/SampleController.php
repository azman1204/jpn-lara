<?php
namespace App\Http\Controllers;
use App\Models\Employee;
use Illuminate\Http\Request;

class SampleController extends Controller
{
    function demo1() {
        return "<h1>Hari ini " . date('d/m/Y');
    }

    function demo2() {
        return view('hello', ['name' => 'John Doe']);
    }

    function demo3() {
        $emps = Employee::all(); // return array of obj. Employee
        //dd($emps); // dd = die dump
        return view('employee.listing', ['emps' => $emps]);
    }
}
