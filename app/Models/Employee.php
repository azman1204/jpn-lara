<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

// model ini by defauly mewakili table bernama employees
// model singular, table plural
class Employee extends Model
{
    use HasFactory;
    public $table = 'employee';
}
